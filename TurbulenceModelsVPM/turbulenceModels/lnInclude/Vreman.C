/*---------------------------------------------------------------------------*\
Vreman - Implementation of the dynamic Vreman
                     SGS model.

Copyright Information
    Copyright (C) 1991-2009 OpenCFD Ltd.
    Copyright (C) 2010-2021 Alberto Passalacqua 

License
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
\*---------------------------------------------------------------------------*/

#include "Vreman.H"
#include "fvOptions.H"
#include "syncTools.H"
#include "volFieldsFwd.H"
#include "fvcAverage.H"
#include "fvc.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{
namespace LESModels
{

// * * * * * * * * * * * * Protected Member Functions  * * * * * * * * * * * //

template<class BasicTurbulenceModel>
void Vreman<BasicTurbulenceModel>::correctNut
(
    const tmp<volTensorField>& gradU
)
{
    const fvMesh& mesh = this->mesh_;
    const volScalarField& nu = this->nu();
    volScalarField deltaSqr = Foam::sqr(this->delta());
    volTensorField beta = T(gradU) & gradU;
    volScalarField Bbeta(Foam::sqr(deltaSqr));
    volScalarField oneF(deltaSqr);
    forAll(deltaSqr, cellI)
    {
        oneF[cellI] = 1.0;
        tensor bec(beta[cellI]);
        Bbeta[cellI] *= (bec.xx() * bec.yy() - bec.xy() * bec.xy() +
                         bec.xx() * bec.zz() - bec.xz() * bec.xz() +
                         bec.yy() * bec.zz() - bec.yz() * bec.yz());
    }
    volScalarField aa = gradU && gradU;
    volScalarField prodg(aa);
    forAll(aa, cellI)
    {
        prodg[cellI] = ((aa[cellI]==0.0)?0.0:(Foam::sqrt((Bbeta[cellI] / aa[cellI]))));
    }
    prodg.correctBoundaryConditions();
    C_.value() = 0.07;
    this->nut_ = C_ * prodg;
    this->nut_.correctBoundaryConditions();
    fv::options::New(this->mesh_).correct(this->nut_);
    BasicTurbulenceModel::correctNut();
}

template<class BasicTurbulenceModel>
void Vreman<BasicTurbulenceModel>::correctNut()
{
    correctNut(this->gradUc_);
}

template<class BasicTurbulenceModel>
void Vreman<BasicTurbulenceModel>::correctNutp()
{
    const fvMesh& mesh = this->mesh_;
    const volVectorField& U = this->U_;
    const pointVectorField& Up = this->Up_;
    const volScalarField& delta = this->delta();
    const pointTensorField& UpGrad = this->UpGrad_;
    const List<scalarRectangularMatrix>& leastSquareSVD = this->leastSquareSVD_;
    boolList isPatchPoint_(mesh.nPoints());
    isPatchPoint_ = false;
    forAll(mesh.boundaryMesh(), patchI)
    {
        const polyPatch& meshPatch = mesh.boundaryMesh()[patchI];

        if(!isA<emptyPolyPatch>(meshPatch) && !mesh.magSf().boundaryField()[patchI].coupled()) 
        {        
            const labelList& pfaceCells = mesh.boundary()[patchI].faceCells();         
            forAll(meshPatch, pfaceI)
            {
                label faceI = mesh.boundary()[patchI].start()+pfaceI;
                const face& facePoint=mesh.faces()[faceI];
                forAll(facePoint, facePointI) isPatchPoint_[facePoint[facePointI]] = true;
            }
        }
    }
    syncTools::syncPointList(mesh, isPatchPoint_, orEqOp<bool>(),false);

    List<scalarField> deltap4(mesh.nPoints());
    forAll(deltap4,pointI)
    {
        deltap4[pointI].setSize(4);
        deltap4[pointI] = 0.0;
    }
    forAll(U,cellI)
    {
        const labelList& pt = mesh.cellPoints()[cellI];
        for (label m = 0; m < pt.size(); m++)
        {
            vector dd = mesh.C()[cellI] - mesh.points()[pt[m]];
            deltap4[pt[m]][0] += delta[cellI];
            deltap4[pt[m]][1] += dd[0] * delta[cellI];
            deltap4[pt[m]][2] += dd[1] * delta[cellI];
            deltap4[pt[m]][3] += dd[2] * delta[cellI];
        }
    }
    forAll(mesh.boundaryMesh(), patchI)
    {
        const polyPatch& meshPatch = mesh.boundaryMesh()[patchI];

        if(!isA<emptyPolyPatch>(meshPatch) && !mesh.magSf().boundaryField()[patchI].coupled())
        {
            const pointField& faceCentres = mesh.faceCentres();
            forAll(meshPatch, pfaceI)
            {
                label faceI = mesh.boundary()[patchI].start()+pfaceI;
                const face& facePoint = mesh.faces()[faceI];
                doubleScalar deltaB = delta.boundaryField()[patchI][pfaceI];
                forAll(facePoint, facePointI)
                {
                    vector dd = faceCentres[faceI] - mesh.points()[facePoint[facePointI]];
                    deltap4[facePoint[facePointI]][0] += deltaB;
                    deltap4[facePoint[facePointI]][1] += dd[0] * deltaB;
                    deltap4[facePoint[facePointI]][2] += dd[1] * deltaB;
                    deltap4[facePoint[facePointI]][3] += dd[2] * deltaB;
                }
            }
        }
    }            
    syncTools::syncPointList(mesh, deltap4, plusEqOp<scalarField>(), deltap4[0]);
    scalarField deltap(mesh.nPoints(), 0.0);
    tensorField UpGradvm(mesh.nPoints(), tensor::zero);
    forAll(deltap, pointI)
    {
        const scalarRectangularMatrix& bSVD = leastSquareSVD[pointI];
        for (label i = 0; i < 4; i++)
        {
            deltap[pointI] += bSVD[0][i] * deltap4[pointI][i];
        }
        UpGradvm[pointI] = UpGrad[pointI];
    }

    scalarField deltaSqr = Foam::sqr(deltap);
    scalarField Bbeta(Foam::sqr(deltaSqr));
    forAll(UpGradvm, pi)
    {
        tensor bec(T(UpGradvm[pi]) & UpGradvm[pi]);
        Bbeta[pi] *= (bec.xx() * bec.yy() - bec.xy() * bec.xy() +
                      bec.xx() * bec.zz() - bec.xz() * bec.xz() +
                      bec.yy() * bec.zz() - bec.yz() * bec.yz());
    }
    scalarField aa = UpGradvm && UpGradvm;
    scalarField prodg(mesh.nPoints(), 0.0);
    forAll(UpGradvm, pI)
    {
        prodg[pI] = ((aa[pI]==0.0)?0.0:(Foam::sqrt((Bbeta[pI] / aa[pI]))));
    }
    forAll(UpGradvm, pI)
    {
        if(isPatchPoint_[pI]) { continue; }
        this->nutp_[pI] = C_.value() * prodg[pI];
    }
    this->nutp_.correctBoundaryConditions();
    BasicTurbulenceModel::correctNutp();
}

// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

template<class BasicTurbulenceModel>
Vreman<BasicTurbulenceModel>::Vreman
(
    const alphaField& alpha,
    const rhoField& rho,
    const volVectorField& U,
    const pointVectorField& Up,
    const volTensorField& gradUc,
    const pointTensorField& UpGrad,
    const List<scalarRectangularMatrix>& leastSquareSVD,
    const surfaceScalarField& alphaRhoPhi,
    const surfaceScalarField& phi,
    const transportModel& transport,
    const word& propertiesName,
    const word& type
)
:
    LESeddyViscosity<BasicTurbulenceModel>
    (
        type,
        alpha,
        rho,
        U,
        Up,
        gradUc,
        UpGrad,
        leastSquareSVD,
        alphaRhoPhi,
        phi,
        transport,
        propertiesName
    ),

    C_("C", (dimArea*dimTime), 0.0),
    filterPtr_(LESfilter::New(U.mesh(), this->coeffDict())),
    filter_(filterPtr_())
{
    if (type == typeName)
    {
        this->printCoeffs(type);
    }
}

// * * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * //

template<class BasicTurbulenceModel>
bool Vreman<BasicTurbulenceModel>::read()
{
    if (LESeddyViscosity<BasicTurbulenceModel>::read())
    {
        filter_.read(this->coeffDict());

        return true;
    }

    return false;
}


template<class BasicTurbulenceModel>
void Vreman<BasicTurbulenceModel>::correct()
{
    if (!this->turbulence_)
    {
        return;
    }

    // Local references
    const alphaField& alpha = this->alpha_;
    const rhoField& rho = this->rho_;
    const surfaceScalarField& alphaRhoPhi = this->alphaRhoPhi_;
    const volVectorField& U = this->U_;
    fv::options& fvOptions(fv::options::New(this->mesh_));

    LESeddyViscosity<BasicTurbulenceModel>::correct();

    tmp<volTensorField> tgradU(this->gradUc_);
    const volTensorField& gradU = tgradU();
    
    correctNut(gradU);
}

template<class BasicTurbulenceModel>
void Vreman<BasicTurbulenceModel>::correctp()
{
    LESeddyViscosity<BasicTurbulenceModel>::correctp();
    correctNutp();
}


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace LESModels
} // End namespace Foam

// ************************************************************************* //
